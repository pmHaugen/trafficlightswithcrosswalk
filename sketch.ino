#include <LiquidCrystal_I2C.h>

const float GAMMA = 0.7;
const float RL10 = 50;
long startTime = millis();
long firstStartTime;
long secondStartTime;
long cycleTime = 20000;
bool bfirstCrossWalk = false;
bool bsecondCrossWalk = false;
LiquidCrystal_I2C lcd(0x27,20,4);

void setup() {
  // put your setup code here, to run once:
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  pinMode(9, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);

  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);

  Serial.begin(9600);

  digitalWrite(12, HIGH);
  digitalWrite(9, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(A0, HIGH);

  pinMode(A3, INPUT);

  pinMode(5, INPUT);
  pinMode(4, INPUT);


  lcd.init();                      // initialize the lcd
  // Print a message to the LCD.
  lcd.backlight();
}
void firstGoGreen(long ET)
{
  //First On
  if(ET > 1000 && ET < 2000)
  {//RED
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);

    digitalWrite(12, HIGH);
    digitalWrite(11, LOW);
    digitalWrite(10, LOW);
    //delay(5000);
  }
    if(ET > 2000 && ET < 3000)
  {//YELLOW RED
    digitalWrite(12, HIGH);
    digitalWrite(11, HIGH);
    digitalWrite(10, LOW);
    //delay(5000);
  }
    if(ET > 3000 && ET < 4000)
  {//GREEN
    digitalWrite(12, LOW);
    digitalWrite(11, LOW);
    digitalWrite(10, HIGH);
    //delay(5000);
  }
}
void firstGoRed(long ET)
{
  //First Off
  if(ET > 5000 && ET < 6000)
  {//GREEN
    digitalWrite(10, HIGH);
    digitalWrite(11, LOW);
    digitalWrite(12, LOW);

    //delay(1000);
  }
  if(ET > 6000 && ET < 7000)
  {//YELLOW
    digitalWrite(10, LOW);
    digitalWrite(11, HIGH);
    digitalWrite(12, LOW);
    //delay(1000);
  }
  if(ET > 7000 && ET < 8000)
  {//RED
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
    digitalWrite(12, HIGH);
  }
}
void secondGoGreen(long ET)
{
  //Second on:
  if(ET > 8000 && ET < 9000)
  {//RED
    digitalWrite(A1, LOW);
    digitalWrite(A0, HIGH);

    digitalWrite(9, HIGH);
    digitalWrite(8, LOW);
    digitalWrite(7, LOW);
    //delay(5000);
  }
    if(ET > 9000 && ET < 10000)
  {//YELLOW RED
    digitalWrite(9, HIGH);
    digitalWrite(8, HIGH);
    digitalWrite(7, LOW);
    //delay(5000);
  }
    if(ET > 10000 && ET < 11000)
  {//GREEN
    digitalWrite(9, LOW);
    digitalWrite(8, LOW);
    digitalWrite(7, HIGH);
    //delay(5000);
  }
}
void secondGoRed(long ET)
{
  //Second off:
  if(ET > 16000 && ET < 17000)
  {//GREEN
    digitalWrite(9, LOW);
    digitalWrite(8, LOW);
    digitalWrite(7, HIGH);

    //delay(1000);
  }
  if(ET > 17000 && ET < 18000)
  {//YELLOW
    digitalWrite(9, LOW);
    digitalWrite(8, HIGH);
    digitalWrite(7, LOW);
    //delay(1000);
  }
  if(ET > 18000 && ET < 19000)
  {//RED
    digitalWrite(9, HIGH);
    digitalWrite(8, LOW);
    digitalWrite(7, LOW);
  }
}
void firstCrossWalk()
{
  digitalWrite(2, HIGH);
  digitalWrite(3, LOW);
  bfirstCrossWalk = false;
}
void secondCrossWalk()
{
  digitalWrite(A1, HIGH);
  digitalWrite(A0, LOW);
  bsecondCrossWalk = false;
}


void loop() {
  long currentTime = millis();
  long elapsedTime = currentTime - startTime;
  if(elapsedTime >= cycleTime)
  {
    Serial.println("Cycle Complete");
    startTime = millis();
  }


  int analogValue = analogRead(A3);
  float voltage = analogValue / 1024. * 5;
  float resistance = 2000 * voltage / (1 - voltage / 5);
  float lux = pow(RL10 * 1e3 * pow(10, GAMMA) / resistance, (1 / GAMMA));
  lcd.setCursor(0,0);
  lcd.print("LUX: ");
  lcd.print(lux);
  if(digitalRead(5) > 0)
  {
      //Serial.println(buttonOne);
      bfirstCrossWalk = true;
  }
  if(digitalRead(4) > 0)
  {
      //Serial.println(buttonTwo);
      bsecondCrossWalk = true;
  }
  if(lux > 100)
  {
  //if(!bfirstCrossWalk)
  firstGoGreen(elapsedTime);
  //if(digitalRead(12) < 1)
  firstGoRed(elapsedTime);
  //if(!bsecondCrossWalk)
  secondGoGreen(elapsedTime);
  secondGoRed(elapsedTime);

if(bfirstCrossWalk && elapsedTime > 8000 && elapsedTime < 9000)
{
  firstCrossWalk();
}
if(bsecondCrossWalk && elapsedTime > 19000)
{
  secondCrossWalk();
}


  }
  else
  {
    delay(300);
    digitalWrite(12, LOW);
    digitalWrite(11, HIGH);
    digitalWrite(10, LOW);

    digitalWrite(3, LOW);
    digitalWrite(2, LOW);

    digitalWrite(9, LOW);
    digitalWrite(8, HIGH);
    digitalWrite(7, LOW);

    digitalWrite(A0, LOW);
    digitalWrite(A1, LOW);
    delay(300);
    digitalWrite(11, LOW);
    digitalWrite(8, LOW);
    //delay (300);
    startTime = millis();
  }
  //Serial.println(elapsedTime);
}
